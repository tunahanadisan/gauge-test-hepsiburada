import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    static WebDriver driver;

    public void Start() {
        System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        this.driver = new ChromeDriver();
        driver.get("https://www.hepsiburada.com/");

    }

    public void Finish() {
    driver.quit();

    }

    public static WebElement findElement(By by) {
        return  driver.findElement(by);

    }

    public  static  void ClickButton(By by) {
        findElement(by).click();

    }
    public static void SendCase(By by,String isim){
       findElement(by).sendKeys(isim);

    }
    public static String isimKontrol(By by){

        return findElement(by).getText();
    }
}
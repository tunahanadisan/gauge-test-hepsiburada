import com.thoughtworks.gauge.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashSet;


public class StepImplementation extends BaseTest {

    WebDriver driver;
    @Test
    @Step("Hesaburada anasayfaya git")
    public void AnasayfaGit() {
        Start();
        System.out.println("hespsiburada sayfası açıldı.");

    }
    @Test
    @Step("Kullanıcı Girisi Butonuna Tıkla")
    public void GirişButonuTıkla() throws InterruptedException {
        ClickButton(By.xpath("//*[@id=\"myAccount\"]/span/span[1]"));
        Thread.sleep(2000);
        ClickButton(By.xpath("//*[@id=\"login\"]"));


    }

    @Test
    @Step("Kullanıcı Bilgilerini Gir <Kullanici_Mail> <Sifre>")
    public void Logın(String Kullanici_Mail, String Sifre) throws InterruptedException {
        Thread.sleep(2000);
        SendCase(By.id("txtUserName"), (Kullanici_Mail));
        SendCase(By.id("txtPassword"), (Sifre));
        ClickButton(By.id("btnLogin"));
        System.out.println("Giris basarili sekilde gerceklesti.");
    }
    @Test
    @Step("Kullanıcı İsmi Kontrol <isim>")
    public void İsimKontrol(String isim) throws InterruptedException {
        Thread.sleep(2000);
        String kullaniciisim = isimKontrol(By.xpath("//*[@id=\"myAccount\"]/span/a/span[2]"));
        System.out.println(kullaniciisim);
        if (isim.equals(kullaniciisim)) {
            System.out.println("isim Eslesti.");
        } else {
            System.out.println("isim Eslesemedi.");

        }


    }
    @Test
    @Step("Hepsiburada Site Kapanıs")
    public void kapanıs() throws InterruptedException {
        Thread.sleep(2000);
        Finish();
        System.out.println("Proje Bitis :)");

    }
}
